# Jack Kerouac's Animated Spell Effects
A small, but growing collection of animated spells for use with various VTT's in the top-down/overhead perspective. All files are in transparent webm format. Files will be located in your "Data\modules\animated-spell-effects\spell-effects" folder. If you like my work, consider saying thanks on Discord: jackkerouac#0624.

Preview/Download: https://gitlab.com/jackkerouac/animated-spell-effects

Requires FVTT version 0.5.0 or higher.

Use this manifest link in Foundry VTT.

https://gitlab.com/jackkerouac/animated-spell-effects/module.json